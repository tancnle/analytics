{% docs umau_28_days_user_desc %}
Number of distinct users who have generated a manage event by month. [link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_28d/20210216180814_events.yml)
{% enddocs %}

{% docs action_monthly_active_users_project_repo_28_days_user_desc %}
Count of monthly active users who have performed any Git operation (read/write/push). [link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_28d/20210216182040_action_monthly_active_users_project_repo.yml)
{% enddocs %}

{% docs merge_requests_28_days_user_desc %}
Count distinct authors of merge requests. [link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_28d/20210216175055_merge_requests.yml)
{% enddocs %}

{% docs projects_with_repositories_enabled_28_days_user_desc %}
Count of users creating projects that have a matching Git repository, result of a Git push action, for last 28 days.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/config/metrics/counts_28d/20210216182049_projects_with_repositories_enabled.yml)
{% enddocs %}

{% docs commit_comment_all_time_event_desc %}
Count of total unique commit comments. Does not include MR diff comments.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210216182004_commit_comment.yml)
{% enddocs %}

{% docs source_code_pushes_all_time_event_desc %}
Count of total Git push operations.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210216182006_source_code_pushes.yml)
{% enddocs %}

{% docs ci_pipelines_28_days_user_desc %}
Distinct users triggering pipelines in a month.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_28d/20210216175554_ci_pipelines.yml)
{% enddocs %}

{% docs ci_internal_pipelines_28_days_user_desc %}
Total pipelines in GitLab repositories in a month.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_28d/20210216175546_ci_internal_pipelines.yml)
{% enddocs %}

{% docs ci_builds_28_days_user_desc %}
Unique monthly builds in project.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_28d/20210216175542_ci_builds.yml)
{% enddocs %}

{% docs ci_builds_all_time_user_desc %}
Unique count of builds in project.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210216175525_ci_builds.yml)
{% enddocs %}

{% docs ci_builds_all_time_event_desc %}
Unique builds in project.[link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210216175510_ci_builds.yml)
{% enddocs %}
